import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class NutrientsWidget extends StatefulWidget {
  const NutrientsWidget({Key? key}) : super(key: key);

  @override
  _NutrientsWidgetState createState() => _NutrientsWidgetState();
}

class _NutrientsWidgetState extends State<NutrientsWidget> {
  var nutrientsValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var nutrients = [
    'ไข่',
    'นม',
    'ถั่ว',
    'ข้าวสาลี',
    'ปลา',
    'กุ้ง',
    'ปู',
    'เจ็บคอ',
    'คัดจมูกน้ำมูกไหล',
    'ผัก',
    'ผลไม้'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Nutrients')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: nutrientsValues[0],
              title: Text(nutrients[0]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[1],
              title: Text(nutrients[1]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[2],
              title: Text(nutrients[2]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[3],
              title: Text(nutrients[3]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[4],
              title: Text(nutrients[4]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[5],
              title: Text(nutrients[5]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[6],
              title: Text(nutrients[6]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[7],
              title: Text(nutrients[7]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[8],
              title: Text(nutrients[8]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[8] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[9],
              title: Text(nutrients[9]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[9] = newValue!;
                });
              }),
          CheckboxListTile(
              value: nutrientsValues[10],
              title: Text(nutrients[10]),
              onChanged: (newValue) {
                setState(() {
                  nutrientsValues[10] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        "[false,false,false,false,false,false,false,false,false,false,false]";
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        nutrientsValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', nutrientsValues.toString());
  }
}
