import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SymptomWidget extends StatefulWidget {
  const SymptomWidget({Key? key}) : super(key: key);

  @override
  _SymptomWidgetState createState() => _SymptomWidgetState();
}

class _SymptomWidgetState extends State<SymptomWidget> {
  var symptomValues = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false
  ];
  var symptom = [
    'ชา หรือคันที่ปาก ใบหู หรือในลำคอ',
    'มีผื่นคันเหมือนเป็นลมพิษ หรือมีผิวหนังแดง รู้สึกคัน แต่ไม่มีผื่น',
    'กลืนอาหารลำบาก',
    'บวมบริเวณใบหน้า เช่น รอบดวงตาม ริมฝีปาก ลิ้น เพดานปาก หรือในลำคอ',
    'วิงเวียนศีรษะ รู้สึกไม่สบาย และคลื่นไส้อาเจียน',
    'มีอากาศปวดท้องน้อย หรือท้องเสีย',
    'มีอากาศคล้ายเป็นไข้ละอองฟาง หรือที่นิยมเรียกกันว่า ภูมิแพ้อากาศ เช่น จาม คันรอบดวงตาและเยื่อบุตา',
    'หายใจติดขัด',
    'คัดจมูก น้ำมูกไหล'
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Symptom')),
      body: ListView(
        children: [
          CheckboxListTile(
              value: symptomValues[0],
              title: Text(symptom[0]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[0] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[1],
              title: Text(symptom[1]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[1] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[2],
              title: Text(symptom[2]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[2] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[3],
              title: Text(symptom[3]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[3] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[4],
              title: Text(symptom[4]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[4] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[5],
              title: Text(symptom[5]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[5] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[6],
              title: Text(symptom[6]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[6] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[7],
              title: Text(symptom[7]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[7] = newValue!;
                });
              }),
          CheckboxListTile(
              value: symptomValues[8],
              title: Text(symptom[8]),
              onChanged: (newValue) {
                setState(() {
                  symptomValues[8] = newValue!;
                });
              }),
          ElevatedButton(
              onPressed: () async {
                await _saveQuestion();
                Navigator.pop(context);
              },
              child: const Text('Save'))
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _loadQuestion();
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        "[false,false,false,false,false,false,false,false,false,false,false]";
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        symptomValues[i] = (arrStrQuestionValues[i].trim() == 'true');
      }
    });
  }

  Future<void> _saveQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('question_values', symptomValues.toString());
  }
}
