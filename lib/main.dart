import 'package:flutter/material.dart';
import 'package:covid_app/profile.dart';
import 'package:covid_app/symptom.dart';
import 'package:covid_app/nutrients.dart';
import 'package:covid_app/treat.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(MaterialApp(
    title: 'food allergy',
    home: MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String name = '';
  String gender = 'M';
  int age = 0;
  var symptomScore = 0.0;
  var riskScore = 0.0;
  var vaccines = ['-', '-', '-'];
  @override
  void initState() {
    super.initState();
    _loadProfile();
    _loadQuestion();
    _loadRisk();
  }

  Future<void> _loadRisk() async {
    var prefs = await SharedPreferences.getInstance();
    var strRiskValue = prefs.getString('risk_values') ??
        "[false,false,false,false,false,false,false]";
    print(strRiskValue.substring(1, strRiskValue.length - 1));
    var arrStrRiskValues =
        strRiskValue.substring(1, strRiskValue.length - 1).split(',');
    setState(() {
      riskScore = 0.0;
      for (var i = 0; i < arrStrRiskValues.length; i++) {
        if (arrStrRiskValues[i].trim() == 'true') {
          riskScore = riskScore + 1;
        }
      }
      riskScore = (riskScore * 100) / 7;
    });
  }

  Future<void> _loadQuestion() async {
    var prefs = await SharedPreferences.getInstance();
    var strQuestionValue = prefs.getString('question_values') ??
        '[false, false, false, false, false, false, false, false, false, false, false]';
    print(strQuestionValue.substring(1, strQuestionValue.length - 1));
    var arrStrQuestionValues =
        strQuestionValue.substring(1, strQuestionValue.length - 1).split(',');
    setState(() {
      symptomScore = 0.0;
      for (var i = 0; i < arrStrQuestionValues.length; i++) {
        if (arrStrQuestionValues[i].trim() == 'true') {
          symptomScore = symptomScore + 1;
        }
      }
      symptomScore = (symptomScore * 100) / 11;
    });
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
    });
  }

  Future<void> _resetProfile() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove('name');
    prefs.remove('gender');
    prefs.remove('age');
    prefs.remove('vaccines');
    prefs.remove('question_values');
    prefs.remove('risk_values');
    await _loadProfile();
    await _loadQuestion();
    await _loadRisk();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Main'),
      ),
      body: ListView(
        children: [
          Card(
            clipBehavior: Clip.antiAlias,
            child: Column(
              children: [
                ListTile(
                  leading: Icon(Icons.person),
                  title: Text(name),
                  subtitle: Text('เพศ: $gender, อายุ: $age ปี',
                      style: TextStyle(color: Colors.black.withOpacity(0.6))),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'อาการ : ${symptomScore.toStringAsFixed(2)} %',
                    style: TextStyle(
                        fontWeight: (symptomScore > 30)
                            ? FontWeight.bold
                            : FontWeight.normal,
                        fontSize: 24.0,
                        color: (symptomScore > 30)
                            ? Colors.red.withOpacity(0.6)
                            : Colors.green.withOpacity(0.6)),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(16),
                  child: Text(
                    'อาการแพ้ : ${riskScore.toStringAsFixed(2)} %',
                  
                  ),
                ),
               
                ButtonBar(
                  alignment: MainAxisAlignment.end,
                  children: [
                    TextButton(
                      onPressed: () async {
                        await _resetProfile();
                      },
                      child: const Text('Reset'),
                    ),
                  ],
                ),
              ],
            ),
          ),
          ListTile(
            title: Text('ข้อมูลส่วนตัว'),
            onTap: () async {
              await Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ProfileWidget()));
              await _loadProfile();
            },
          ),
          ListTile(
              title: Text('สารอาหาร(ก่อนเกิดอาการ)'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NutrientsWidget()));
                await _loadProfile();
            },
          ),
          ListTile(
              title: Text('อาการที่แพ้'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => SymptomWidget()));
                await _loadQuestion();
            },
          ),
          ListTile(
              title: Text('ข้อมูลการรักษา'),
              onTap: () async {
                await Navigator.push(context,
                    MaterialPageRoute(builder: (context) => TreatWidget()));
                await _loadRisk();
            },
          ),
        ],
      ),
    );
  }
}
